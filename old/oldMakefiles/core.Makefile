CPP = clang
CPPFLAGS =-std=c++0x -lboost_system -Wall -Wextra -g

all: event.o player.o clean

event.o: event.cpp event.h
	$(CPP) -c $(CPPFLAGS) event.cpp

player.o: player.cpp player.h
	$(CPP) -c $(CPPFLAGS) player.cpp

clean:
	rm *.o
