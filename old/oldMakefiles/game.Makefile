CPP = clang
BINDIR = $(PWD)/../../bin/game
DEBUGDIR = $(BINDIR)/debug
STD=-std=c++0x
CPPFLAGS = $(STD) -lboost_system -lsfml-system -lsfml-graphics -lsfml-window -Wall -Wextra -g

all: game.out clean

game.out: game.o console.o chatbuffer.o event.o 
	$(CPP) -o $(DEBUGDIR)/game.out $(CPPFLAGS) ../core/event.o game.o console.o chatbuffer.o

game.o: game.cpp game.h
	$(CPP) -c $(CPPFLAGS) game.cpp

console.o: console.cpp console.h
	$(CPP) -c  $(CPPFLAGS) console.cpp

chatbuffer.o: chatbuffer.cpp chatbuffer.h
	$(CPP) -c $(CPPFLAGS) chatbuffer.cpp

event.o: ../core/event.cpp ../core/event.h
	cd ../core && make event.o

clean:
	rm *.o && cd ../core && make clean
