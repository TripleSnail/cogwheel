CPP = g++
BINDIR = $(PWD)/../../bin/server
DEBUGDIR = $(BINDIR)/debug
CPPFLAGS = -lsfml-system -lsfml-network -Wall -Wextra -g

all: server.out client.out clean

server.out: server.o
	$(CPP) $(CPPFLAGS) -o $(DEBUGDIR)/server.out server.o

server.o: server.cpp server.h
	$(CPP) -c server.cpp

client.out: testclient.o
	$(CPP) $(CPPFLAGS) -o $(DEBUGDIR)/client.out testclient.o

testclient.o:
	$(CPP) $(CPPFLAGS) -c testclient.cpp

clean:
	rm *.o
