#include "event.h"
#include <iostream>

EventManager::EventManager(bool setAsGlobal)
   : IEventManager(setAsGlobal)
{
   activeQueue_ = 0;
}

EventManager::~EventManager()
{
}

bool EventManager::AddListener(EventListenerPtr const & listener, EventType type)
{
   // I'm not 100% sure why we have this first iterator
   EventTypeSet::iterator evtIter = typeSet_.find(type);
   EventListenerMap::iterator evtLMapIter = eventRegistry_.find(type);
   
   if (evtLMapIter == eventRegistry_.end())
   {
      EventListenerMapRes evtLMapRes = eventRegistry_.insert(EventListenerEntry(type, EventListenerTable()));
      if (evtLMapRes.second == false)
         return false;
      if (evtLMapRes.first == eventRegistry_.end())
         return false;
      
      evtLMapIter = evtLMapRes.first;
   }
   EventListenerTable& evtLTable = evtLMapIter->second;
   
   for (auto iter = evtLTable.begin(), iterEnd = evtLTable.end(); iter != iterEnd ; iter++)
   {
      bool listenerMatches = (*iter == listener);
      if (listenerMatches)
         return false;
   }
   
   evtLTable.push_back(listener);

   // std::cout << "Listener pushed to table\n";
   return true;
}

bool EventManager::DeleteListener(EventListenerPtr const & listener, EventType const & type)
{
   // What is rc you ask? I don't know
   bool rc = false;

   for (auto iter = eventRegistry_.begin(), iterEnd = eventRegistry_.end(); iter != iterEnd; iter++ )
   {
      unsigned int const eventId = iter->first;
      EventListenerTable& table = iter->second;
      
      for (auto iter2 = table.begin(), iter2End = table.end(); iter2 != iter2End; iter2++)
      {
         if (*iter2 == listener)
         {
            table.erase(iter2);
            rc = true;
            break;
         }     
      }
   }
   return rc;
}

bool EventManager::Trigger(IEventData const & event) const
{
   EventListenerMap::const_iterator iterOtus = eventRegistry_.find(0);
   
   if (iterOtus != eventRegistry_.end())
   {
      EventListenerTable const & table = iterOtus->second;
      // bool processed = false;
      
      for (EventListenerTable::const_iterator iter2 = table.begin(), iter2End = table.end()
              ; iter2 != iter2End; iter2++)
      {
         (*iter2)->HandleEvent(event);
      }
   }
   EventListenerMap::const_iterator iter = eventRegistry_.find(event.GetEventType());
   
   if (iter == eventRegistry_.end())
      return false;
   
   EventListenerTable const & table = iter->second;
   
   bool processed = false;
   
   for (EventListenerTable::const_iterator iter2 = table.begin(), iter2End = table.end()
           ; iter2 != iter2End; iter2++)
   {
      EventListenerPtr listener = *iter2;
      if (listener->HandleEvent(event))
      {
         processed = true;
      }
      
   }
   return processed;
}

bool EventManager::QueueEvent(IEventDataPtr const & event)
{
   EventListenerMap::const_iterator iter = eventRegistry_.find(event->GetEventType());
   
   if (iter == eventRegistry_.end())
   {
      // In book: itWC. What is "WC" supposed to mean?!?
      EventListenerMap::const_iterator iterOtus = eventRegistry_.find(0);
      if (iterOtus == eventRegistry_.end())
         return false;
   }
   eventQueues_[activeQueue_].push_back(event);
   std::cout << event->GetEventType() << std::endl;
   return true;
}

bool EventManager::AbortEvent(EventType const & type, bool allOfType)
{
   EventListenerMap::iterator iter = eventRegistry_.find(type);
   
   if (iter == eventRegistry_.end())
      return false;
   
   // In book: rc. What does that mean??
   bool otus = false;
   
   EventQueue& evtQueue = eventQueues_[activeQueue_];
   
   for (EventQueue::iterator iter2 = evtQueue.begin(), iterEnd = evtQueue.end()
           ; iter2 != iterEnd; iter2++)
   {
      if ((*iter2)->GetEventType() == type)
      {
         iter2 = evtQueue.erase(iter2);
         otus = true;
         if (!allOfType)
            break;
      }
      else
         ++iter2;
      
   }

   return otus;
}

bool EventManager::Tick(unsigned long maxMilliSeconds)
{
   // std::chrono::steady_clock::time_point now =
   // std::chrono::steady_clock::now() currentMilliSeconds =
   // boost::chrono::steady_clock::now() - ;
   boost::chrono::milliseconds start;
   unsigned long currentMilliSeconds = start.count();
   unsigned long maxMilliSecs = (maxMilliSeconds == IEventManager::INFINITE) ? maxMilliSeconds :
      (currentMilliSeconds + maxMilliSeconds);
   
   EventListenerMap::const_iterator iterOtus = eventRegistry_.find(0);
   int queueToProcess = activeQueue_;
   activeQueue_ = (activeQueue_ + 1) % numOfQueues;
   eventQueues_[activeQueue_].clear();
   
   while (eventQueues_[queueToProcess].size() > 0)
   {
      IEventDataPtr event = eventQueues_[queueToProcess].front();
      eventQueues_[queueToProcess].pop_front();
      
      const EventType eventType = event->GetEventType();

      // if (eventType == E_MESSAGE_ENTERED)
      //    std::cout << "\"E_MESSAGE_ENTERED\" -event found from
      //    event queue\n";

      // std::cout << eventType << std::endl;
      std::cout << "Otus\n";
      
      
      EventListenerMap::const_iterator iterListeners = eventRegistry_.find(eventType);

      if (iterOtus != eventRegistry_.end())
      {
         const EventListenerTable& table = iterOtus->second;
         
         for (EventListenerTable::const_iterator iter = table.begin(), iterEnd = table.end()
                 ; iter != iterEnd ; iter++)
         {
            (*iter)->HandleEvent(*event);
         }
      }
      
      if (iterListeners == eventRegistry_.end())
         continue;
      
      // unsigned int const eventId = iterListeners->first;
      const EventListenerTable& table = iterListeners->second;
      
      for (EventListenerTable::const_iterator iter = table.begin(), iterEnd = table.end()
              ; iter != iterEnd; iter++)
      {
         if ((*iter)->HandleEvent(*event))
            break;
      }
      boost::chrono::milliseconds end;
      currentMilliSeconds = end.count();
      
      if (maxMilliSeconds != IEventManager::INFINITE)
         if (currentMilliSeconds >= maxMilliSecs)
            break;
   }

   bool queueFlushed = eventQueues_[queueToProcess].size() == 0;

   if (!queueFlushed)
      while (eventQueues_[queueToProcess].size() > 0)
      {
         IEventDataPtr event = eventQueues_[queueToProcess].back();
         eventQueues_[queueToProcess].pop_back();
         eventQueues_[activeQueue_].push_front(event);
      }
   // std::cout << activeQueue_ << std::endl;
   
   return queueFlushed;
}

EventListenerList EventManager::GetListOfListeners(EventType const & type) const
{
   EventListenerMap::const_iterator iterListeners = eventRegistry_.find(type);
   
   if (iterListeners == eventRegistry_.end())
      return EventListenerList();
   
   const EventListenerTable& table = iterListeners->second;
   
   if (table.size() == 0)
      return EventListenerList();

   EventListenerList result;
   result.reserve(table.size());
   
   for (EventListenerTable::const_iterator iter = table.begin(), iterEnd = table.end();
        iter != iterEnd; iter++)
      result.push_back(*iter);
   
   return result;
   
}

static IEventManager *evtManagerPtr = 0;

IEventManager * IEventManager::Get() 
{
   return evtManagerPtr;
}

IEventManager::IEventManager(bool setAsGlobal) 
{
   if (setAsGlobal)
   {
      evtManagerPtr = this;
   }
}

IEventManager::~IEventManager() 
{
   if (evtManagerPtr == this)
      evtManagerPtr = 0;
}



// int main(int argc, char *argv[])
// {
//    std::cout << "Testing event.cpp\n";
//    return 0;
// }
