#ifndef _EVENT_H_
#define _EVENT_H_

#include <vector>
#include <map>
#include <set>
#include <list>
#include <queue>
#include <deque>
#include <string>
#include <utility>
#include <memory>
#include <boost/chrono.hpp>

// This code has slight taste of copy-pasta

// TODO: Replace enum with something smarter like hashes.
// See "Game Coding Complete"
enum EventType
{
   E_CONSOLE_ACTIVATED,
   E_CONSOLE_DEACTIVATED,
   E_MESSAGE_ENTERED,
   E_MESSAGE_RECEIVED
};

class IEventData
{
public:
   // IEventData();
   // virtual ~IEventData();
   virtual EventType GetEventType() const = 0;
};

class BaseEventData: public IEventData
{
public:
   explicit BaseEventData() {}
   virtual ~BaseEventData() {}
   virtual EventType GetEventType() const = 0;

// protected:
   
};

typedef std::shared_ptr<IEventData> IEventDataPtr;

class IEventListener
{
public:
   explicit IEventListener() {};
   virtual ~IEventListener() {};
   
   virtual bool HandleEvent(IEventData const & evt) { return true; }
};

typedef std::shared_ptr<IEventListener> EventListenerPtr;

class IEventManager
{
public:
   enum eventConstants {INFINITE = 0xffffffff}; 
      
   explicit IEventManager(bool setAsGlobal); 
   virtual ~IEventManager() = 0;
   virtual bool AddListener(EventListenerPtr const & handler, EventType type) = 0;
   virtual bool DeleteListener(EventListenerPtr const & handler, EventType const & type) = 0;
   virtual bool Trigger(IEventData const & event) const = 0;
   virtual bool QueueEvent(IEventDataPtr const & Event) = 0;
   virtual bool AbortEvent(EventType const & type, bool allOfType = false) = 0;
   virtual bool Tick(unsigned long maxMilliSeconds = INFINITE) = 0;
   
// private:
   static IEventManager * Get(); // TODO: Make private and make helper
                                 // functions for better encapsulation
}; 

typedef std::vector<EventListenerPtr> EventListenerList;
typedef std::vector<EventType> EventTypeList;


// class IEventManager;

class EventManager : public IEventManager
{
public:
   explicit EventManager(bool setAsGlobal);
   virtual ~EventManager();
   bool AddListener(EventListenerPtr const & listener, EventType type);
   bool DeleteListener(const EventListenerPtr& listener, const EventType& type);
   bool Trigger(const IEventData& event) const;
   bool QueueEvent(const IEventDataPtr& event);
   bool AbortEvent(const EventType& type, bool allOfType = false);
   bool Tick(unsigned long maxMilliSeconds);
   EventListenerList GetListOfListeners(const EventType& type) const;
   
private:
   typedef std::set<EventType> EventTypeSet;
   typedef std::pair<EventTypeSet::iterator, bool> EventTypeSetRes;
   typedef std::list<EventListenerPtr> EventListenerTable;
   typedef std::map<unsigned int, EventListenerTable> EventListenerMap;
   typedef std::pair<unsigned int, EventListenerTable> EventListenerEntry;
   typedef std::pair<EventListenerMap::iterator, bool> EventListenerMapRes;
   typedef std::list<IEventDataPtr> EventQueue;

   EventTypeSet typeSet_;
   EventListenerMap eventRegistry_;
   // TODO: Use double buffering
   const static int numOfQueues = 2;
   EventQueue eventQueues_[2];
   int activeQueue_;
};

   
#endif /* _EVENT_H_ */
