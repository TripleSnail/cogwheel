#ifndef _EVENTS_H_
#define _EVENTS_H_

#include "event.h"
#include <string>

// class BaseEventData;

struct EMessageEntered: public BaseEventData
{
   virtual EventType GetEventType() const
   {
      return E_MESSAGE_ENTERED;
   }
   
   explicit EMessageEntered(std::string const & enteredMsg)
      : msg(enteredMsg)
   {
   }
   
   std::string msg;
};

struct EMessageReceived: public BaseEventData
{
   virtual EventType GetEventType() const
   {
      return E_MESSAGE_RECEIVED;
   }
   
   explicit EMessageReceived(const std::string sendersNick, const std::string& messageReceived)
      : nick(sendersNick), msg(messageReceived)
   {
   }

   std::string nick;
   std::string msg;
};

#endif /* _EVENTS_H_ */
