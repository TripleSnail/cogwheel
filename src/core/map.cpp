#include "map.h"

// Tile //=================================================

Tile::Tile()
{
   // size_ = 0;
}

Tile::~Tile()
{
}

bool Tile::Initialize(const sf::Image& image_)
{
   // Load map here
   return true;
}

// void Tile::SetPosition(unsigned int x, unsigned int y)
// {
   
// }


TileType Tile::GetTileType() const
{
   return type_;
}

void Tile::Draw()
{
}

// Map //========================================================

Map::Map()
{
}

Map::~Map()
{
}

bool Map::Initialize(sf::RenderWindow * wind)
{
   assert(wind);
   wind_ = wind;
   return true;
}

Tile& Map::GetTile(unsigned int x, unsigned int y) const
{
   assert(coords.x >= width_ || coords.y >= height_);
   return tileMatrix_[coords.x][coords.y];
}

bool Map::SetTile(Tile& tile, unsigned int x, unsigned int y)
{
   assert(x >= width_ || y >= height_);
   tile.SetPosition(GetPositionPixel(x, y));
   tileMatrix_[x][y] = tile;
   
}

void Map::Draw()
{
   for (int x = 0; x < width_; ++x)
      for (int y = 0; y < height_; ++y)
         wind_->Draw(tileMatrix_[x][y].sprite_);
}

// void Map::DrawTile(unsigned int x, unsigned int y)
// {
   
// }

inline
sf::Vector2f GetPositionPixel(unsigned int x, unsigned int y) const
{
   float size = Tile::GetTileSize();
   return sf::Vector2f(x * size, y * size);
}
