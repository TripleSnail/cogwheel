#ifndef _MAP_H_
#define _MAP_H_

#include <vector>
#include <cassert>
#include <SFML/Graphics.hpp>

enum TileType
{
   T_EMPTY,
   T_GRASS
};

typedef struct Coordinates
{
   int x;
   int y;
} Coordinates;

class Tile
{
   friend Map;

public:
   Tile();
   virtual ~Tile();
   virtual bool Initialize();
   // void SetPosition(unsigned int x, unsigned int y);
   TileType GetTileType() const;
   
private:
   // All tiles are supposed to be square shaped and have the same size right?
   static float size_;
   TileType type_;
};

class DrawableTile: public Tile
{
public:
   DrawableTile();
   virtual ~DrawableTile();
   static SetTileSize(float size) { size_ = size; }
   static GetTileSize() const { return size_;} ;
   bool Initialize(sf::Image&);   

private:
   sf::Image * image_;
   sf::Sprite texture_;
   
};   

class Map
{
public:
   Map();
   virtual ~Map();
   bool Initialize(sf::RenderWindow* wind);
   bool LoadMap();
   Tile& GetTile(unsigned int x, unsigned int y) const;
   Tile& GetTile(const Coordinates& coords) const;
   void Draw();

private:
   typedef std::vector<std::vector<Tile> > TileMatrix;
   TileMatrix tileMatrix_;
   int width_;
   int height_;
   sf::RenderWindow* wind_;
   // void DrawTile(unsigned int x, unsigned int y);
   bool SetTile(Tile&, unsigned int x, unsigned int y);
   sf::Vector2f GetPositionPixel(unsigned int x, unsigned int y) const;
   
};

#endif /* _MAP_H_ */
