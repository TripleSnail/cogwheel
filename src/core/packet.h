// #include <string>
// #include <SFML/Network.hpp>

#ifndef _PACKET_H_
#define _PACKET_H_

// Every packet to server shall have following information (in this order):
// PacketType > Nickname

typedef sf::Int8 PacketTypeInt;


enum PacketType 
{
   P_KEEPALIVE = 0,
   P_REGISTER_NICK = 1,
   P_MESSAGE = 2,
   P_QUIT_SERVER = 3 
};

// In addition, different packets can have extra information. Here is
// the table:

// To edit this table, please remove comments and use orgtbl-mode

// | PacketType    | Information     |
// |---------------+-----------------|
// | P_KEEPALIVE   | Nothing         |
// | P_MESSAGE     | string: message |
// | P_QUIT_SERVER | Nothing yet     |
// |               |                 |
// |               |                 |
// |               |                 |
// |               |                 |
// |               |                 |
// |               |                 |

#endif /* _PACKET_H_ */
