#include "chatbuffer.h"
#include <cassert>

struct EMessageEntered;

ChatBuffer::ChatBuffer(int linesToShow,
                       float lineSpace,
                       float textHeight,
                       float leftX,
                       float bottomY,
                       sf::Color defaultColor)
{
   linesToShow_ = linesToShow;
   lineSpace_ = lineSpace;
   textHeight_ = textHeight;
   leftX_ = leftX;
   bottomY_ = bottomY;
   defaultColor_ = defaultColor;
   // defaultFont_ = font;
}

ChatBuffer::~ChatBuffer()
{
   
}

bool ChatBuffer::Initialize()
{
   evtManagerPtr_ = IEventManager::Get();
   assert(evtManagerPtr_);
   EventListenerPtr evtListenerPtr(this);
   evtManagerPtr_->AddListener(evtListenerPtr, E_MESSAGE_RECEIVED);
   return true;
}

bool ChatBuffer::SetAssets(const sf::Font& font)
{
   defaultFont_ = font;
}

void ChatBuffer::AddNewMessage(const string& fromNick, const string& message)
{
   if (visibleText_.size() == linesToShow_)
      visibleText_.pop_back();

   // cout << message << std::endl;
   std::string strToAdd = "<" + fromNick + "> " + message;
   cout << strToAdd << std::endl;
   messages_.push_front(strToAdd);
   sf::String textToAdd(strToAdd, defaultFont_, textHeight_);
   
   textToAdd.SetColor(defaultColor_);
   visibleText_.push_front(textToAdd);

   int i = 0;
   cout << "Size of visibleText_: " << visibleText_.size() << std::endl;
   
   for (list<sf::String>::iterator iter = visibleText_.begin(), iterEnd = visibleText_.end()
           ; iter != iterEnd; iter++, ++i)
   {
      float x = leftX_;
      float y = bottomY_ - i * lineSpace_ - textHeight_;

      iter->SetPosition(x, y);
      cout << "Setp string number " << i << " position to x: " << x << " y: " << y << std::endl;
   }
}

bool ChatBuffer::HandleEvent(IEventData const & evt)
{
   cout << "HandleEvent called!\n";
   bool isSuccesful = true;
   EventType evtType = evt.GetEventType();

   if (evtType == E_MESSAGE_ENTERED)
   {
      const EMessageEntered& castEvent = static_cast<const EMessageEntered&>(evt);
      cout << "Chatbuffer detected 'E_MESSAGE_ENTERED'-event\n";
      // cout << castEvent.msg << std::endl;
      // AddNewMessage(castEvent.msg);
   }
   
   else if (evtType == E_MESSAGE_RECEIVED)
   {
      const EMessageReceived& castEvent = static_cast<const EMessageReceived&>(evt);
      cout << "Chatbuffer detected 'E_MESSAGE_RECEIVED'-event\n";
      std::string fromNick = castEvent.nick;
      std::string message = castEvent.msg;
      
      AddNewMessage(fromNick, castEvent.msg);
   }
   
   return isSuccesful;
}

// void ChatBuffer::Update()
// {
   
// }

void ChatBuffer::Draw(sf::RenderWindow& wind)
{
   for (list<sf::String>::const_iterator iter = visibleText_.begin(), iterEnd = visibleText_.end()
           ; iter != iterEnd; iter++)
   {
      wind.Draw(*iter);     
   }
}
