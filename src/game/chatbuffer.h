#ifndef _CHATBUFFER_H_
#define _CHATBUFFER_H_

#include <iostream>
#include <string>
#include <deque>
#include <queue>
#include <list>
// #include <memory>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include "../core/event.h"
#include "../core/events.h"

using std::deque;
using std::string;
using std::cout;
using std::list;


class ChatBuffer: public IEventListener
{
   friend class Game;

public:
   ChatBuffer(int linesToShow,
              float lineSpace,
              float textHeight,
              float leftX,
              float bottomY,
              sf::Color defaultColor_);
   virtual ~ChatBuffer();
   bool Initialize();
   bool SetAssets(const sf::Font&);
   void AddNewMessage(const string& fromNick, const string& message);
   bool HandleEvent(IEventData const & evt);
   // void Clean();

protected:
   list<string> messages_;
   list<sf::String> visibleText_;
   // unsigned int bufferSize_;
   unsigned int maxBufferSize_;
   int linesToShow_;
   float lineSpace_;
   float textHeight_;
   float leftX_;
   float bottomY_;
   float fontSize_;
   sf::Color defaultColor_;
   sf::Font defaultFont_;
   // sf::Color own
   virtual void Draw(sf::RenderWindow&);
   IEventManager * evtManagerPtr_;
   
};


#endif /* _CHATBUFFER_H_ */
