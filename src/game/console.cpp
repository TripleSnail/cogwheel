#include "console.h"
#include <cstdio>

using std::cout;

Console::Console(const Game& game_)
{
   active_ = false;
   textBuffer_ = "";
   maxLength_ = 70;
   // font_ = font;
}

Console::~Console()
{
}

bool Console::Initialize()
{
   text_.SetColor(sf::Color(255, 255, 255));
   text_.SetRotation(0.0f);
   text_.SetScale(1.0f, 1.0f);
   text_.Move(100.0f, 200.0f);
   text_.SetSize(15.0f);
   evtManagerPtr_ = IEventManager::Get();
   assert(evtManagerPtr_);
   return true;
}

bool Console::SetAssets(const sf::Font& font)
{
   font_ = font;
   text_.SetFont(font_);
   return true;
}


inline bool Console::IsActive() const
{
   return active_;
}

// bool Console::IEventListener

bool Console::Toggle()
{
   if (IsActive()) 
   {
      active_ = false;
      cout << "Console deactivated!\n";
      return false;
   }

   else
   {
      active_ = true;
      cout << "Console activated!\n";
      return true;
   }
}

void Console::EventLoop(const sf::Event& evt)
{
   if ((evt.Type == sf::Event::KeyPressed) && (evt.Key.Code == sf::Key::F11))
   {
      Toggle();
   }
   
   if (IsActive() && (evt.Type == sf::Event::TextEntered) )
   {
      char enteredChar = static_cast<char>(evt.Text.Unicode);
      
      if (enteredChar == 0x8) // backspace
      {
         unsigned int textLen = textBuffer_.length();
         textBuffer_ = textBuffer_.substr(0, textLen - 1);
      }
      
      else if (evt.Text.Unicode < 128 && textBuffer_.length() < maxLength_)
      {
         textBuffer_ += static_cast<char>(evt.Text.Unicode);         
         // std::cout << textBuffer_ << std::endl;
      }
   }

   if (IsActive() &&
       (evt.Type == sf::Event::KeyPressed) && 
       (evt.Key.Code == sf::Key::Return) &&
       (textBuffer_.length() > 0))
   {
      // IEventDataPtr eventDataPtr(new EMessageEntered(textBuffer_));
      // cout << "\n";
      Send();
      textBuffer_ = "";
      text_.SetText("");
      // evtManager_->QueueEvent(EMessageEntered(textBuffer_));
   }
}

void Console::Update()
{
   text_.SetText(textBuffer_);
}
 
void Console::Draw(sf::RenderWindow& wind)
{  
   if (IsActive())
   {
      wind.Draw(text_);
   }
}

void Console::Send()
{  
   if (!textBuffer_.empty())
   {     
      evtManagerPtr_->QueueEvent(IEventDataPtr(new EMessageEntered(textBuffer_)));
   }  
}
