#ifndef _CONSOLE_H_
#define _CONSOLE_H_

#include <cassert>
#include <iostream>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include "../core/event.h"
#include "../core/events.h"

/* #include "game.h" */

class Game;

class Console
{
   friend class Game;
   
   /* friend void Game::Update(); */

public:
   Console(const Game& game);
   ~Console();
   bool IsActive() const;
   
private:
   bool Initialize();
   bool SetAssets(const sf::Font&);
   void EventLoop(const sf::Event&);
   void Update();
   void Draw(sf::RenderWindow&);
   bool Toggle();
   void Send();
   bool active_;
   int maxLength_;
   std::string textBuffer_;
   sf::Font font_;
   sf::String text_;
   Game * game_;
   IEventManager * evtManagerPtr_;
   
};

// struct EMessageEntered: public BaseEventData
// {
//    virtual EventType GetEventType() const
//    {
//       return E_MESSAGE_ENTERED;
//    }
   
//    explicit EMessageEntered(std::string const & enteredMsg)
//       : msg(enteredMsg)
//    {
//       std::cout << "E_MESSAGE_ENTERED -event created!\n";
//       // std::cout << "Message is: " << msg << std::endl;
      
//    }

//    std::string msg;  
// };


#endif /* _CONSOLE_H_ */
