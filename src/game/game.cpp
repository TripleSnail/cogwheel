#include "game.h"

using std::cout;
using std::endl;

// For testing purposes we shall use some (static) global variables
// Let's prefix global variables with g_ until we get rid of them.
// Remember, they are used only to make testing easier
static std::string g_nickname;
static sf::IPAddress g_ipAddress;
static short g_port;
static bool g_testing_networking;


Game::Game()
   : // evtManagerPtr_(new EventManager(true)),
     console_(*this), chatBuffer_(8, 20.0f, 15.0f, 10.0f, 200.0f, sf::Color(255, 0, 0))
{
   // evtManagerPtr_ = new EventManager(true);
   evtManagerPtr_ = new EventManager(true);
   networkManagerPtr_ = new NetworkManager();
   // console_= Console::Console(*this);
   // chatBuffer_= ChatBuffer::ChatBuffer(8, 15.0f, 15.0f, 10.0f, 100.0f, sf::Color(255, 0, 0))

   assert(evtManagerPtr_);
   assert(networkManagerPtr_);
}

Game::~Game()
{
}

void Game::Start()
{
   mainWindow_.Create(sf::VideoMode(800, 600, 32), "COGWHEEL");
   Initialize();
   LoadAssets();
   MainLoop();
}

void NextMiniGame()
{

}

bool Game::Initialize()
{
   if (g_testing_networking)
   {
      networkManagerPtr_->Initialize(g_ipAddress, g_port, g_nickname);
      networkManagerPtr_->ConnectToServer();
      networkManagerPtr_->RegisterNickName();
   }
   
   console_.Initialize();
   chatBuffer_.Initialize();
   return true;
}

bool Game::LoadAssets()
{
   
   if (!basicFont_.LoadFromFile("../assets/fonts/FreeSans.ttf", 50))
   {
      std::cout << "Error loading font. Closing game\n";
      gameState_ = EXITING;
      return false;
   }

   if (!testImg_.LoadFromFile("../assets/images/avatar-3d-gnu.png"))
   {
      std::cout << "Error loading ../assets/images/avatar-3d-gnu.png. Closing game\n";
      gameState_ = EXITING;
      return false;
   }
   
   testSprite_.SetImage(testImg_);
   console_.SetAssets(basicFont_);
   chatBuffer_.SetAssets(basicFont_);

   return true;
}

void Game::EventLoop()
{
   evtManagerPtr_->Tick();
   sf::Event event;
   
   while (mainWindow_.GetEvent(event))
   {
      console_.EventLoop(event);
      if (event.Type == sf::Event::Closed)
      {
         gameState_ = EXITING;
         mainWindow_.Close();
      }
      if ((event.Type == sf::Event::KeyPressed) && (event.Key.Code == sf::Key::Escape))
      {
         gameState_ = EXITING;
         mainWindow_.Close();
      }
   }
}

void Game::MainLoop()
{
   if (g_testing_networking)
      networkManagerPtr_->Launch();
   
   while (!IsExiting())
   {
      EventLoop();
      Update();
      Draw();
   }

   networkManagerPtr_->Wait();
}

void Game::Update()
{
   console_.Update();  
}

void Game::Draw()
{
   mainWindow_.Clear();
   // Drawing stuff goes here
   mainWindow_.Draw(testSprite_);
   console_.Draw(mainWindow_);
   chatBuffer_.Draw(mainWindow_);
   mainWindow_.Display();
}

bool Game::IsExiting()
{
   if (gameState_ == EXITING)
      return true;
   else
      return false;
}

int main(int argc, char *argv[])
{
   if (argc == 4)
   {
      g_ipAddress = sf::IPAddress(argv[1]);
      g_port = atoi(argv[2]);
      g_nickname = std::string(argv[3]);
      std::cout << "\nIP:Port " << g_ipAddress << ":" << g_port << ", nickname: " << g_nickname << std::endl;
      g_testing_networking = true;
   }
   else
   {
      g_testing_networking = false;
      std::cout << "Testing without networking \n";
   }
   
   Game game;
   game.Start();
   return 0;
}
