#ifndef _GAME_H_
#define _GAME_H_

#include <iostream>
#include <queue>
#include <cassert>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
// #include <memory>
#include "console.h"
#include "chatbuffer.h"
#include "../core/event.h"
#include "networkmanager.h"

// Get rid of this later
#include <cstdlib>


/* class Console; */

class Game
{
  public:
   Game();
   ~Game();
   void Start();
   void NextMiniGame();
   /* void DrawObject(const sf::Drawable& drawableObj); */
   

  private:
   bool IsExiting();
   bool Initialize();
   bool LoadAssets();
   void EventLoop();
   void Update();
   void Draw();
   void MainLoop();
   enum GameState { UNINITIALIZED, INTRODUCTION, PAUSED, PLAYING, MINI_GAME_OVER, EXITING};
   GameState gameState_;
   sf::RenderWindow mainWindow_;
   sf::Font basicFont_;
   sf::Image testImg_;
   sf::Sprite testSprite_;
   Console console_;
   ChatBuffer chatBuffer_;
   IEventManager *evtManagerPtr_;
   NetworkManager *networkManagerPtr_;
};

#endif /* _GAME_H_ */
