#ifndef _IGAMEOBJECT_H_
#define _IGAMEOBJECT_H_

#include <SFML/System.hpp>

// Not really useful yet

class IGameObject
{
  public:
virtual IGameObject(Game * game) = 0;
virtual ~IGameObject() = 0;
virtual void Update() = 0;

}
#endif /* _IGAMEOBJECT_H_ */
