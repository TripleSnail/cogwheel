#include "networkmanager.h"

using std::cout;

static NetworkManager* networkManagerPtr = 0;

NetworkManager::NetworkManager()
{
   connected_ = false;
   networkManagerPtr = this;
}

NetworkManager::~NetworkManager()
{
}

bool NetworkManager::Initialize(sf::IPAddress& ipAddr, short port, std::string& nickname)
{
   evtManagerPtr_ = IEventManager::Get();
   assert(evtManagerPtr_);
   EventListenerPtr evtListenerPtr(this);
   evtManagerPtr_->AddListener(evtListenerPtr, E_MESSAGE_ENTERED);
   clientAddr_ = sf::IPAddress::GetLocalAddress();
   serverAddr_ = ipAddr;
   port_ = port;
   nickname_ = nickname;
   return true;
}

bool NetworkManager::RegisterNickName()
{
   sf::Packet packet;
   PacketTypeInt typeInt = static_cast<PacketTypeInt>(P_REGISTER_NICK);
   packet << typeInt;
   packet << nickname_;
   socket_.Send(packet);
   return true;
}

bool NetworkManager::ConnectToServer()
{
   if (socket_.Connect(port_, serverAddr_) != sf::Socket::Done)
   {
      cout << "Failed to connect to server\n";
   }

   connected_ = RegisterNickName();
   return connected_;
}

void NetworkManager::Run()
{
   while (connected_)
   {
      cout << "Updating NetworkManager\n";
      sf::Packet keepAlivePacket;
      PacketTypeInt keepAlivePacketTypeInt = static_cast<PacketTypeInt>(P_KEEPALIVE);

      keepAlivePacket << keepAlivePacketTypeInt << nickname_;
      if (socket_.Send(keepAlivePacket) != sf::Socket::Done)
      {
         connected_ = false;
         cout << "Connection to server lost\n";
      }
      
      cout << "Keep-alive-packet sent\n";
      
      sf::Packet packetFromServer;
      
      if (socket_.Receive(packetFromServer) != sf::Socket::Done)
      {
         connected_ = false;
         cout << "Problem receiving packets from server\n";
      }
      
      else
      {
         cout << "Got packet from server\n";
         PacketTypeInt typeInt;
         packetFromServer >> typeInt;   
         PacketType type = static_cast<PacketType>(typeInt);
         
         if (type == P_MESSAGE)
         {
            cout << "NetworkManager received packet with type 'P_MESSAGE'\n";
            std::string fromNick;
            packetFromServer >> fromNick;
            std::string message;
            packetFromServer >> message;
            
            evtManagerPtr_->QueueEvent(IEventDataPtr(new EMessageReceived(fromNick, message)));
         }
      }  
   }
}

bool NetworkManager::HandleEvent(const IEventData& evt)
{
   bool isSuccesful = true;
   EventType evtType = evt.GetEventType();
   sf::Int8 packetTypeInt;

   if (evtType ==  E_MESSAGE_ENTERED)
   {      
      const EMessageEntered& castEvent = static_cast<const EMessageEntered&>(evt);
      cout << "NetworkManager detected 'E_MESSAGE_ENTERED'-event\n";
      sf::Packet packet;

      packetTypeInt = static_cast<sf::Int8>(P_MESSAGE);
      packet << packetTypeInt;
      
      packet << nickname_;
      packet << castEvent.msg;
      connected_ = (socket_.Send(packet) == sf::Socket::Done);
   }
   
   // else if (evtType == E_MESSAGE_RECEIVED)
   // {
   //    //    const EMessageReceived& castEvent = static_cast<const EMessageReceived&>(evt);
   //    //    cout << "NetworkManager 
   // }

   return isSuccesful;
}

bool NetworkManager::SendToServer()
{
   return true;
}

NetworkManager* NetworkManager::Get()
{
   return networkManagerPtr;
}
