#ifndef _NETWORKMANAGER_H_
#define _NETWORKMANAGER_H_

#include <iostream>
#include <SFML/Network.hpp>
#include <string>
#include <cassert>
#include "../core/event.h"
#include "../core/events.h"
#include "../core/packet.h"


class NetworkManager: public IEventListener, public sf::Thread
{
public:
   NetworkManager();
   virtual ~NetworkManager();
   // IP, port and nickname are initialized here until we have better
   // mechnism for that
   bool Initialize(sf::IPAddress&, short port, std::string& nickname);
   bool RegisterNickName();
   bool ConnectToServer();
   virtual void Run();
   bool HandleEvent(const IEventData& evt);
   static NetworkManager* Get();
   
private:
   bool SendToServer();
   sf::IPAddress clientAddr_;
   sf::IPAddress serverAddr_;
   sf::SocketTCP socket_;
   unsigned short port_;
   std::string nickname_;
   IEventManager * evtManagerPtr_;
   bool connected_;
   
};

#endif /* _NETWORKMANAGER_H_ */
