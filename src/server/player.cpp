#include "player.h"

Player::Player(const std::string& nick, const sf::SocketTCP& socket, const sf::IPAddress& ip)
{
   nickName_ = nick;
   socket_ = socket;
   ipAddr_ = ip;
}

string Player::GetNickName() const
{
   return nickName_;
}

sf::IPAddress Player::GetIpAddr() const
{
   // Validate request
      return ipAddr_;
}

sf::SocketTCP Player::GetSocket()
{
   return socket_;
}


void Player::SetIpAddr(sf::IPAddress& ipAddr)
{
   bool isAuthorizedRequest = true;
   bool isValidIp = true;
   if (isAuthorizedRequest && isValidIp)
   {
      ipAddr_ = ipAddr;
   }
}

inline
bool Player::IsActive() const
{
   return active_;
}

void Player::SetIsActive(bool active)
{
   if (IsActive() && !active)
   {
      // Everything is right
      active_ = false;
   }
   
   else if (!IsActive() && active)
   {
      // Everything is right
      active_ = true;
   }
   

   else if (IsActive() && active)
   {
      cout << "Trying to set already active player as active\n";
      active_ = true;
   }

   else if (!IsActive() && !active)
   {
      cout << "Trying to set inactive player as inactive\n";
      active_ = false;
   }
   
}

      
