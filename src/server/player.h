#ifndef _PLAYER_H_
#define _PLAYER_H_

#include <string>
#include <iostream>
#include <SFML/Network.hpp>

// #include <SFML>

using std::string;
using std::cout;

class Player 
{     
   
public:
   Player(const std::string& nick, const sf::SocketTCP& socket, const sf::IPAddress& ip);
   ~Player()
   {  }
   // const string& GetNickName() const;
   string GetNickName() const;
   void SetIpAddr(sf::IPAddress&);
   bool IsActive() const;
   void SetIsActive(bool active);
   sf::IPAddress GetIpAddr() const;
   sf::SocketTCP GetSocket();
   
private:
   string nickName_;
   sf::IPAddress ipAddr_;
   sf::SocketTCP socket_;
   bool hostOrNot_;
   bool active_;
};

#endif /* _PLAYER_H_ */
