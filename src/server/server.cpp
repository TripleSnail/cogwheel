#include "server.h"

using std::cout;

Server::Server()
{
}

Server::~Server()
{
}

bool Server::EstablishConnection(sf::SocketTCP& sock)
{
   bool success = false;
   sf::IPAddress ip;
   sf::SocketTCP client;
   if (listener_.Accept(client, &ip) != sf::Socket::Done)
   {
      cout << "Problem connecting\n";
      return false;
   }

   cout << "Client connected!\n" << "Address: " << ip << std::endl;
   selector_.Add(client);

   sf::Packet registrationPacket;
   if (client.Receive(registrationPacket) != sf::Socket::Done)
   {
      cout << "Problem receiving registration packet\n";
      return false;
   }
            
   else
   {
      sf::Int8 packetTypeInt;
      PacketType packetType;
      registrationPacket >> packetTypeInt;
      packetType = (PacketType)packetTypeInt;
      
      string nick;
      registrationPacket >> nick;
      cout << "Nick to register is: " << nick << std::endl;
      players_.insert(PlayerEntry(nick, Player(nick, client, ip)));
      success = true;
   }
            
   return success;
}

void Server::SendToAll(std::string& nick, std::string& message)
{
   sf::Packet toClients;
   sf::Int8 packetTypeInt = static_cast<sf::Int8>(P_MESSAGE);

   toClients << packetTypeInt;   
   toClients << nick;
   toClients << message;

   for (PlayerEntry playerEntry : players_)
   {      
      Player player = playerEntry.second;
      sf::SocketTCP socket = player.GetSocket();
      
      cout << "- Trying to send packet to player: " << playerEntry.first << std::endl;
      
      if (socket.Send(toClients) != sf::Socket::Done)
      {
         cout << "Failed to send packet\n";
      }

      else
      {
         cout << "Sending packet succesfull\n";
      }
   }
}

void Server::HandlePacket(sf::Packet& packet)
{
   sf::Int8 packetTypeInt;
   PacketType packetType;

   packet >> packetTypeInt;
   packetType = (PacketType)packetTypeInt;

   std::string nick;
   packet >> nick;

   PlayerMap::iterator playerMapIter = players_.find(nick);
   Player* player = 0;

   if (playerMapIter != players_.end())
      player = &playerMapIter->second;
   else
   {
      cout << "No player called " << nick << " logged to server\n";
   }
   
   assert(player);
   if (packetType == P_KEEPALIVE)
      cout << "P_KEEPALIVE\n";

   else if (packetType == P_MESSAGE)
   {
      cout << "packetType is P_MESSAGE\n";
      std::string msg;
      packet >> msg;
      cout << '<' << player->GetNickName() << "> " << msg << std::endl;
      SendToAll(nick, msg);
   }
}


void Server::MainLoop()
{
   if (!listener_.Listen(12345))
      std::cout << "Error\n";

   std::cout << "Listening to port " << "12345" << ", waiting for connections..." << std::endl;
   selector_.Add(listener_);

   while (true)
   {
      unsigned int nbSockets = selector_.Wait();

      for (unsigned int i = 0; i < nbSockets; ++i)
      {
         sf::SocketTCP sock = selector_.GetSocketReady(i);    
        
         if (sock == listener_)
         {
            EstablishConnection(sock);
         }

         else
         {
            sf::Packet packet;
            if (sock.Receive(packet) == sf::Socket::Done)
            {
               HandlePacket(packet);
            }

            else
            {
               cout << "Error! (Some kind of..)\n";
            }
         }
      }
   }
}

int main()
{
   Server server;
   server.MainLoop();
   return 0;
}
