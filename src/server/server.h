#ifndef _SERVER_H_
#define _SERVER_H_

#include <iostream>
#include <SFML/Network.hpp>
#include "../core/event.h"
#include "player.h"
#include "../core/packet.h"

#include <map>
#include <set>
#include <vector>
#include <string>
#include <memory>
#include <cassert>


class Server 
{
public:
   Server();
   ~Server();
   bool EstablishConnection(sf::SocketTCP&);
   void MainLoop();
private:
   typedef std::map<std::string, Player> PlayerMap;
   typedef std::pair<std::string, Player> PlayerEntry;

   void HandlePacket(sf::Packet&);
   void SendToAll(std::string& nick, std::string& message);

   sf::IPAddress serverIp_;
   sf::SocketTCP listener_;
   sf::SelectorTCP selector_;
   PlayerMap players_;
};

#endif /* _SERVER_H_ */
