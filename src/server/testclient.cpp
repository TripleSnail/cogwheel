#include <iostream>
#include <string>

#include <SFML/System.hpp>
#include <SFML/Network.hpp>

#include "../core/packet.h"

using std::cout;
using std::cin;

// Since we won't be using testclient.cpp for long, it's okay to use
// some global variables

bool connected = false;
sf::SocketTCP sock;

inline
void RequestNicknameRegistration(std::string& nickname)
{
   sf::Packet pack;
   // unsigned int packetTypeInt = (unsigned int)P_REGISTER_NICK;
   sf::Int8 packetTypeInt = static_cast<sf::Int8>(P_REGISTER_NICK);
   
   pack << packetTypeInt;
   pack << nickname;   
   sock.Send(pack);
}

void EnableChat(void * nickPtr)
{
   std::string nickname = *static_cast<std::string*>(nickPtr);
   while (connected)
   {
      std::string message;
      std::getline(cin, message);
      
      if (!message.empty())
      {  
         sf::Packet pack;
         // unsigned int packetTypeInt = (unsigned int)P_MESSAGE;
         sf::Int8 packetTypeInt = static_cast<sf::Int8>(P_MESSAGE);

         pack << packetTypeInt;
         pack << nickname;
         pack << message;
         connected = (sock.Send(pack) == sf::Socket::Done);     
      }
   }
}


void DoClient(std::string ipAddr, unsigned short port)
{
   sf::IPAddress ip(ipAddr);

   cout << "Please enter your nickname:\n";
   std::string nickname;
   std::getline(cin, nickname);

   if (sock.Connect(port, ip) != sf::Socket::Done)
      return;

   RequestNicknameRegistration(nickname);
   connected = true;
   sf::Thread chatThread(&EnableChat, &nickname);
   chatThread.Launch();

   while (connected)
   {
      sf::Packet keepAlivePacket;
      // unsigned int keepAlivePacketTypeInt = (unsigned
      // int)P_KEEPALIVE;
      sf::Int8 keepAlivePacketTypeInt = static_cast<sf::Int8>(P_KEEPALIVE);
      
      // keepAlivePacket << (int)P_KEEPALIVE << nickname;
      keepAlivePacket << keepAlivePacketTypeInt << nickname;

      if (sock.Send(keepAlivePacket) != sf::Socket::Done)
      {
         connected = false;
         cout << "C0nnection to server lost\n";
      }
      
      sf::Packet packetFromServer;
      
      if (sock.Receive(packetFromServer) != sf::Socket::Done)
      {
         connected = false;
         cout << "Problem receiving packets from server\n";
      }
      
      else
      {
         sf::Int8 packetTypeInt;
         std::string fromNick;
         
         PacketType type;
         std::string chatMessage;

         packetFromServer >> packetTypeInt;
         type = static_cast<PacketType>(packetTypeInt);

         packetFromServer >> fromNick;
         packetFromServer >> chatMessage;
         cout << fromNick << " says: " << chatMessage << std::endl;
         // cout << "chatMessage: " << chatMessage << ", type: " << type << std::endl;
      }
   }
   sock.Close();
}

int main ()
{  
   DoClient("127.0.0.1", 12345);  
   return 0;
}
